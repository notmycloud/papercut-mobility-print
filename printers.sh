#!/bin/bash

### configure CUPS (background subshell, wait till cups http is running...)

until cupsctl -h localhost:${CUPS_PORT:-631} > /dev/null 2>&1; do echo -n "."; sleep 1; done;
#until cupsctl --debug-logging > /dev/null 2>&1; do echo -n "."; sleep 1; done;
cupsctl --remote-admin
cupsctl --share-printers
# Wait for CUPS to reload
sleep 1
echo "--> CUPS ready"

set -e

# setup printers (run each CUPS_LPADMIN_PRINTER* command)
echo "--> adding printers"
for v in $(set |grep ^CUPS_LPADMIN_PRINTER |sed -e 's/^\(CUPS_LPADMIN_PRINTER[^=]*\).*/\1/' |sort |tr '\n' ' '); do
    echo "$v = $(eval echo "\$$v")"
    eval $(eval echo "\$$v")
done
echo "--> Printers added to CUPS"

cat <<EOFcupsd
===================================================================================================================
The dockerized CUPS instance is now ready for use!

The web interface is available here:
URL:                http://$(hostname -i):${CUPS_PORT:-631}/ or http://${CUPS_SERVER_NAME:-$(hostname -f)}:${CUPS_PORT:-631}/
Username:           ${CUPS_ADMIN_USER:-admin}
Default Password:   $(hostname -f)

===================================================================================================================
EOFcupsd

exit 0