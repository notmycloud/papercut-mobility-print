#!/bin/bash

### /entrypoint.sh

### Exit immediately if a command exits with a non-zero status.
set -ev




### Configure Papercut
# Change DNS port from 9153 to 53
sed -i 's/9153/53/' /home/papercut/pc-mobility-print/pc-mobility-print.conf
# Configure auto-updates
/home/papercut/pc-mobility-print/updaterconfigtool auto-update -enable="${PAPERCUT_UPDATE_ENABLE:-true}"

### Test the Papercut configuration
echo "Validating the Papercut configuration..."
/home/papercut/pc-mobility-print/pc-mobility-print validate





### Admin User Setup
echo "Configuring CUPS Admin user..."
CUPS_ADMIN_USER="${CUPS_ADMIN_USER:-admin}"
CUPS_ADMIN_PASSWORD="${CUPS_ADMIN_PASSWORD:-$(hostname -f)}"

### Check for valid ASCII password
if printf '%s' "${CUPS_ADMIN_PASSWORD}" | LC_ALL=C grep -q '[^ -~]\+'; then
  RETURN=1; REASON="CUPS password contain illegal non-ASCII characters, aborting!"; exit;
fi

### Create admin user
if [ $(grep -ci ${CUPS_ADMIN_USER} /etc/shadow) -eq 0 ]; then
    #useradd "${CUPS_ADMIN_USER}" --system -G lp,sys --no-create-home --password $(mkpasswd ${CUPS_ADMIN_PASSWORD})
    useradd "${CUPS_ADMIN_USER}" --system -G lp,sys --no-create-home --password "$(htpasswd -nb2 ${CUPS_ADMIN_USER} ${CUPS_ADMIN_PASSWORD} | cut -d ':' -f2)"
    if [[ ${?} -ne 0 ]]; then RETURN=${?}; REASON="Failed to set password [REDACTED] for user ${CUPS_ADMIN_USER}, aborting!"; exit; fi
fi





### Populate default Cups config
### Does not override existing files.
cp -rn /etc/cups.default/* /etc/cups/




### Execute the Cups Configuration Scripts
if [ -z "${CUPS_DISABLE_CONFIG}" ] || [ "${CUPS_DISABLE_CONFIG}" = "false" ]; then
    echo "Configuring the CUPS service..."
    bash /cupsd-config.sh
    bash /cups-files.sh
fi





### Test the configuration
echo "Validating the CUPS configuration..."
/usr/sbin/cupsd -t

### List available lpadmin options
### Only works when printers are added.
#/usr/bin/lpoptions -l





echo "Starting Systemd..."
exec "$@"