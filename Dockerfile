# docker build -t container_tag --build-arg UBI_IMAGE_VERSION_ARG=8.7-9 --build-arg PC_MOBILITY_PRINT_VERSION_ARG=1.0.3512 .

ARG UBI_IMAGE_VERSION_ARG=8.7-9
FROM registry.redhat.io/ubi8/ubi-init:${UBI_IMAGE_VERSION_ARG}
LABEL org.opencontainers.image.authors="devops@notmy.cloud"
LABEL org.opencontainers.image.url="https://gitlab.com/notmycloud/ubi-papercut-mobility-print"
LABEL org.opencontainers.image.source="https://gitlab.com/notmycloud/ubi-papercut-mobility-print/-/blob/main/Dockerfile"

### Configure Journald for Container Logging
### TODO: Forward to host Journal
RUN sed -i 's/^.*ForwardToConsole.*$/ForwardToConsole=yes/' /etc/systemd/journald.conf




### Install Cups Print Server
RUN dnf install -y \
        cups \
    && dnf clean all \
    && rm -rf /var/cache /var/log/dnf* /var/log/yum.* \
    && systemctl enable cups.service

### Install misc dependancies
### - hostname for IP address and Hostname detection in configuration script.
### - httpd-tools for htpasswd used when creating a new user in configuration script.
RUN dnf install -y \
        hostname \
        httpd-tools \
    && dnf clean all \
    && rm -rf /var/cache /var/log/dnf* /var/log/yum.*





### Set default configuration values for Cups
### - Add the lp group to the @SYSTEM group.
### - Backup Cups configuration for later use when /etc/cups is bind mounted.
#RUN sed -i '/SystemGroup/{s/:$/:lp/;t;s/$/ lp/}' /etc/cups/cups-files.conf \
#    && sed -i '/SystemGroup/s/^#//' /etc/cups/cups-files.conf \
RUN mkdir -p /etc/cups.default \
    && cp -r /etc/cups/* /etc/cups.default/





# https://www.papercut.com/help/manuals/mobility-print/set-up/installation/#download-and-install-mobility-print-on-your-print-server

### Add the Papercut User
### - Add user to the wheel group for sudo access
### - Limits configuration per Papercut documentation
RUN useradd -m papercut \
    && if [ -z "$(grep papercut /etc/security/limits.conf)" ]; then \
        echo "papercut - nofile 65535" >> /etc/security/limits.conf; \
    fi

### Add Papercut dependancies
### - Ghostscript Papercut Windows printing
### - Sudo for Papercut installer
### - Allow Papercut user to Sudo
RUN dnf install -y \
        ghostscript \
        sudo \
    && dnf clean all \
    && rm -rf /var/cache /var/log/dnf* /var/log/yum.* \
    && cat > /etc/sudoers.d/papercut <<< "papercut ALL=(ALL:ALL) NOPASSWD:ALL" \
    && chmod -R 440 /etc/sudoers.d \
    && visudo -c





USER papercut
WORKDIR /home/papercut

### Download the Papercut install files
ARG PC_MOBILITY_PRINT_VERSION_ARG=1.0.3512
ENV PC_MOBILITY_PRINT_VERSION $PC_MOBILITY_PRINT_VERSION_ARG
LABEL version=$PC_MOBILITY_PRINT_VERSION_ARG

### Download & Install the Papercut Mobility
RUN curl "https://cdn.papercut.com/web/products/mobility-print/installers/server/linux/pc-mobility-print-${PC_MOBILITY_PRINT_VERSION}.sh" -o pc-mobility-print.sh \
    && bash pc-mobility-print.sh --non-interactive -v \
    && rm pc-mobility-print.sh





USER root
WORKDIR /

### Setup the entrypoint script(s)
COPY --chmod=744 entrypoint.sh /
COPY --chmod=744 cupsd-config.sh /
COPY --chmod=744 cups-files.sh /
COPY --chmod=744 printers.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/sbin/init"]

COPY lpadmin.service /etc/systemd/system/
RUN systemctl enable lpadmin.service





### Persist the Cups configuration
VOLUME "/etc/cups"

### Persist Print Jobs
VOLUME "/var/spool/cups"

### Persist Papercut Configuration
VOLUME "/home/papercut/pc-mobility-print/data/config"





### Expose service ports
# Cups Dashboard
EXPOSE 631/tcp
# Papercut HTTP
EXPOSE 9163/tcp
# Papercut HTTPS/SSL
EXPOSE 9164/tcp
# Papercut DNS
EXPOSE 53/udp
# Papercut MDNS
EXPOSE 5353/udp





### Setup the healthcheck
### CHMOD requires BuildKit
COPY --chmod=777 healthcheck.sh /
# HEALTHCHECK is unsupported in Podman Build
HEALTHCHECK --start-period=30s --interval=10s --timeout=3s --retries=3 CMD /healthcheck.sh