#!/bin/bash

# /healthcheck.sh

# Exit script on first error
set -e
# Show lines being executed
set -v


# Check Papercut Web Interface:
curl -q -S -s -k -f -o /dev/null http://127.0.0.1:9163 || exit 1
curl -q -S -s -k -f -o /dev/null https://127.0.0.1:9164 || exit 1

# Check Cups Web Interface:
curl -I -q -S -s -k -f -o /dev/null https://127.0.0.1:631/printers/ || exit 1

# Check Cups API
cupsctl < /dev/null || exit 1

# Check LP Stat
if [ "$(lpstat -r)"  != "scheduler is running" ]; then
    exit $?
fi

exit 0
