#!/bin/bash

### /cups-files.sh

### Exit immediately if a command exits with a non-zero status.
set -ev

CONFIG_FILE="/etc/cups/cups-files.conf"

func_cups_option () {
    if [[ -z "${2}" ]]; then
        # If no value given, comment out the entry
        echo "${CONFIG_FILE}: Disabling ${1}"
        #sed -e "/${1}/ s/^/#/" -i "${CONFIG_FILE}"
        #sed -e "/ ?${1}/ s/^#*/#&/" -i "${CONFIG_FILE}"
        sed -i "s/^${1}/#&/" "${CONFIG_FILE}"
        return
    fi

    findLine=$(grep -E "^#? ?${1}" "${CONFIG_FILE}" || true)
    if [ -z "${findLine}" ]; then
        echo "${CONFIG_FILE}: Adding [${1} ${2}]"
        echo "${1} ${2}" >> "${CONFIG_FILE}"
    else
        # Using the ~ as a separator character to avoid issues with file paths.
        echo "${CONFIG_FILE}: Configuring [${findLine} --> ${1} ${2}]"
        sed -i "s~^#? ?${1} .*~${1} ${2}~" "${CONFIG_FILE}"
    fi
}





### Setup custom SSL Certificates
### Specifies whether the scheduler automatically creates self-signed certificates for client connections using TLS.
### The default is yes.
if [ -n "${CUPS_SSL_CERT}" -a -n "${CUPS_SSL_KEY}" ]; then
    func_cups_option CreateSelfSignedCerts no

    # Populate the provided certificate into the configuration.
    CUPS_SERVER_KEYCHAIN="${CUPS_SERVER_KEYCHAIN:-/etc/cups/ssl}"
    echo -e "${CUPS_SSL_CERT}" > "${CUPS_SERVER_KEYCHAIN}/${CUPS_SERVER_NAME:-$(hostname -f)}.crt"
    echo -e "${CUPS_SSL_KEY}" > "${CUPS_SERVER_KEYCHAIN}/${CUPS_SERVER_NAME:-$(hostname -f)}.key"
else
    func_cups_option CreateSelfSignedCerts yes
fi





### Configuration options below will be set to an opinionated default for the Papercut Mobility scenario.
### The description will list the CUPS default.
### https://www.cups.org/doc/man-cups-files.conf.html

### Defines the access log filename.
### Specifying a blank filename disables access log generation.
### The value "stderr" causes log entries to be sent to the standard error file when the scheduler is running in the foreground, or to the system log daemon when run in the background.
### The value "syslog" causes log entries to be sent to the system log daemon.
### The server name may be included in filenames using the string "%s", for example: AccessLog /var/log/cups/%s-access_log
### The default is "/var/log/cups/access_log".
func_cups_option AccessLog "${CUPS_ACCESS_LOG:-stderr}"

### Specifies the directory to use for long-lived temporary (cache) files.
### The default is "/var/spool/cups/cache" or "/var/cache/cups" depending on the platform.
func_cups_option CacheDir "${CUPS_CACHE_DIR:-}"

### Specifies the permissions for all configuration files that the scheduler writes.
### The default is "0644" on macOS and "0640" on all other operating systems.
func_cups_option ConfigFilePerm "${CUPS_CONFIG_FILE_PERM:-}"

### Specifies the directory where data files can be found.
### The default is usually "/usr/share/cups".
func_cups_option DataDir "${CUPS_DATA_DIR:-}"

### Specifies the root directory for the CUPS web interface content.
### The default is usually "/usr/share/doc/cups".
func_cups_option DocumentRoot "${CUPS_DOCUMENT_ROOT:-}"

### Defines the error log filename.
### Specifying a blank filename disables error log generation.
### The value "stderr" causes log entries to be sent to the standard error file when the scheduler is running in the foreground, or to the system log daemon when run in the background.
### The value "syslog" causes log entries to be sent to the system log daemon.
### The server name may be included in filenames using the string "%s", for example: ErrorLog /var/log/cups/%s-error_log
### The default is "/var/log/cups/error_log".
func_cups_option ErrorLog "${CUPS_ERROR_LOG:-stderr}"

### Specifies which errors are fatal, causing the scheduler to exit.
### The default is "config". The kind strings are:
### none: No errors are fatal.
### all: All of the errors below are fatal.
### browse: Browsing initialization errors are fatal, for example failed connections to the DNS-SD daemon.
### config: Configuration file syntax errors are fatal.
### listen: Listen or Port errors are fatal, except for IPv6 failures on the loopback or "any" addresses.
### log: Log file creation or write errors are fatal.
### permissions: Bad startup file permissions are fatal, for example shared TLS certificate and key files with world-read permissions.
func_cups_option FatalErrors "${CUPS_FATAL_ERRORS:-}"

### Specifies the group name or ID that will be used when executing external programs.
### The default group is operating system specific but is usually "lp" or "nobody".
func_cups_option Group "${CUPS_GROUP:-}"

### Specifies the permissions of all log files that the scheduler writes.
### The default is "0644".
func_cups_option LogFilePerm "${CUPS_LOG_FILE_PERM:-}"

### Defines the page log filename.
### The value "stderr" causes log entries to be sent to the standard error file when the scheduler is running in the foreground, or to the system log daemon when run in the background.
### The value "syslog" causes log entries to be sent to the system log daemon.
### Specifying a blank filename disables page log generation.
### The server name may be included in filenames using the string "%s", for example: PageLog /var/log/cups/%s-page_log
### The default is "/var/log/cups/page_log".
func_cups_option PageLog "${CUPS_PAGE_LOG:-stderr}"

### Passes the specified environment variable(s) to child processes.
### Note: the standard CUPS filter and backend environment variables cannot be overridden using this directive.
func_cups_option PassEnv "${CUPS_PASS_ENV:-}"

### Specifies the username that is associated with unauthenticated accesses by clients claiming to be the root user.
### The default is "remroot".
func_cups_option RemoteRoot "${CUPS_REMOTE_ROOT:-}"

### Specifies the directory that contains print jobs and other HTTP request data.
### The default is "/var/spool/cups".
func_cups_option RequestRoot "${CUPS_REQUEST_ROOT:-}"

### Specifies the level of security sandboxing that is applied to print filters, backends, and other child processes of the scheduler.
### The default is "strict".
### This directive is currently only used/supported on macOS.

### Specifies the directory containing the backends, CGI programs, filters, helper programs, notifiers, and port monitors.
### The default is "/usr/lib/cups" or "/usr/libexec/cups" depending on the platform.
func_cups_option ServerBin "${CUPS_SERVER_BIN:-}"

### Specifies the location of TLS certificates and private keys.
### The default is "/Library/Keychains/System.keychain" on macOS and "/etc/cups/ssl" on all other operating systems.
### macOS uses its keychain database to store certificates and keys while other platforms use separate files in the specified directory, *.crt for PEM-encoded certificates and *.key for PEM-encoded private keys.
func_cups_option ServerKeychain "${CUPS_SERVER_KEYCHAIN:-}"

### Specifies the directory containing the server configuration files.
### The default is "/etc/cups".
func_cups_option ServerRoot "${CUPS_SERVER_ROOT:-}"

### Set the specified environment variable to be passed to child processes.
### Note: the standard CUPS filter and backend environment variables cannot be overridden using this directive.
for v in $(set |grep ^CUPS_SET_ENV |sed -e 's/^\(CUPS_SET_ENV[^=]*\).*/\1/' |sort |tr '\n' ' '); do
    echo "SetEnv=$(eval echo "\$$v")"
    echo "SetEnv $(eval echo "\$$v")" >> /etc/cups/cups-files.conf
done

### Specifies the directory to use for PID and local certificate files.
### The default is "/var/run/cups" or "/etc/cups" depending on the platform.
func_cups_option StateDir "${CUPS_STATE_DIR:-}"

### Specifies whether the scheduler calls fsync(2) after writing configuration or state files.
### The default is "No".
func_cups_option SyncOnClose "${CUPS_SYNC_ON_CLOSE:-Yes}"

### Specifies the group(s) to use for @SYSTEM group authentication.
### The default contains "admin", "lpadmin", "root", "sys", and/or "system".
### We will add listed groups to the SystemGroup list
if [ -n "${CUPS_SYSTEM_GROUPS}" ]; then
    # If no value given, comment out the entry
    echo "Adding ${CUPS_SYSTEM_GROUPS} to the CUPS SystemGroup"
    sed -i "/SystemGroup/{s/:$/:${CUPS_SYSTEM_GROUPS}/;t;s/$/ ${CUPS_SYSTEM_GROUPS}/}" /etc/cups/cups-files.conf
fi

### Specifies the directory where short-term temporary files are stored.
### The default is "/var/spool/cups/tmp".
func_cups_option TempDir "${CUPS_TEMP_DIR:-}"

### Specifies the user name or ID that is used when running external programs.
### The default is "lp".
func_cups_option User "${CUPS_USER:-}"