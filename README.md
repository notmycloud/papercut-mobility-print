# Papercut Mobility Print

This image combines a CUPS server with Papercut's Mobility Print.

## VOLUMES
These paths in the container store notable files and are marked as a VOLUME in the build process, but you may want to mount them elsewhere.

CUPS server configuration, I would only mount this elsewhere if you need to provide your own configuration that our entrypoint does not handle.
- /etc/cups/

CUPS Print jobs, mount this if you want to persist print jobs between rebuilding the container.
- /var/spool/cups/

Papercut Configuration, I would definately mount this so you don't have to reconfigure Papercut all of the time.
- /home/papercut/pc-mobility-print/data/config/

## PORTS
Cups Dashboard:     `631/tcp`  
Papercut HTTP:      `9163/tcp`  
Papercut HTTPS/SSL: `9164/tcp`  
Papercut DNS:       `53/udp`  
Papercut MDNS:      `5353/udp`

## HEALTHCHECK
This image is preconfigured with a simple healthcheck at `/healthcheck.sh`.

## SPECIAL NOTES
I have found that in order to properly see all of the logs once the entrypoint hands off to Systemd, use the `-t` flag to provide a TTY.

The entrypoint will configure an `admin` user with a default password of the container ID.
These are configurable, see the ENV section for more details.

If you mount to `/etc/cups/` the entrypoint will copy default files into that directory, without overwriting any that already exist.
If you don't want a file overwritten, just create an empty file in its place.

You can disable our configuration of the CUPS server with an ENV variable.

The entrypoint will validate the configuration of both CUPS and Papercut before handing off to Systemd.

Papercut will auto-update, this can be disabled with an ENV variable.

## Adding Printers
To add printers, you will need to mount `/lpadmin.env` into the container.

You can add as many printers as you want simply by incrementing the number at the end of the variable.

See the lpadmin [man](https://www.man7.org/linux/man-pages/man8/lpadmin.8.html) page for more information.

```bash
#CUPS_LPADMIN_PRINTER=lpadmin -p PRINTER_NAME -m DRIVER -v URI -D OPTIONAL_DESCRIPTION -L OPTIONAL_LOCATION -E
CUPS_LPADMIN_PRINTER1=lpadmin -p North_Office -m everywhere -o printer-is-shared=true -o cupsIPPSupplies=true -v ipp://192.168.255.12/ipp -D "Color MFC" -L "North Office Area" -E
CUPS_LPADMIN_PRINTER2=lpadmin -p Exec_Offices -m everywhere -o printer-is-shared=true -o cupsIPPSupplies=true -v ipp://192.168.255.11/ipp -D "Executive Office Printer" -L "Executive Office Area" -E
```

### DOCKER/PODMAN

Basic usage: 
```bash
podman run \
    --name papercut \
    -d \
    -t \
    --restart no \
    --replace \
    --rm \
    --stop-timeout 60 \
    -v /etc/localtime:/etc/localtime:ro \
    -v ./lpadmin.env:/lpadmin.env:ro \
    -v ./spool/:/var/spool/cups/:rw \
    -v ./config/:/home/papercut/pc-mobility-print/data/config:rw \
    --env-file ./papercut.env \
    --systemd=always \
    docker.io/notmycloud/papercut-mp:latest
```

### VARIABLES
See the following files for variables that you can use, I will create a list in the near future.
- entrypoint.sh
- cups-files.sh
- cupsd-config.sh